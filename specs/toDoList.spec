ToDoList Liste Kontrol
=====================

ToDoList'e is ekleme
----------------
*Listenin bos oldugunu kontrol et
*Listeye "Proje yapilacak" ekle
*Listede "Proje yapilacak" var mi

ToDoList'e birden fazla yapilacak is ekle siralarini kontrol et
--------------------------------------------------------------------
*Yapilacaklar listesinde "Proje yapilacak" iteminin oldugundan emin ol
*Listeye "Bilgisayari al" ekle
*Listede "Bilgisayari al" itemi "Proje yapilacak" iteminin altinda mi

ToDoList radio buton isaretle
-------------------------------
*Listede "Proje yapilacak" ve "Bilgisayari al" itemi varmı
*Listedeki "Proje yapilacak" iteminin radio butonuna tikla
*Listedeki "Proje yapilacak" itemi isaretlendi mi

ToDoList radio buton isaret kaldir
------------------------------------
*Listede "Proje yapilacak" iteminin isaretli oldugunu kontrol et
*Listedeki "Proje yapilacak" iteminin radio butonuna tikla
*Listedeki "Proje yapilacak" itemi isaretsiz mi

ToDoList yapilacak is silme
---------------------------
*Listede "Proje yapilacak" ve "Bilgisayari al" itemi varmı
*Listeye "Herseyi duzenli yap" ekle
*Listede "Herseyi duzenli yap" var mi
*Listedeki "Proje yapilacak" iteminin silme butonuna tikla
*Listede "Proje yapilacak" silindi mi